# newlienr

Processes an input stream of the form;
```
07 27 07 27 ...
```
Into a new stream containing newlines;
```
07\n
27\n
07\n
27\n
...
```
