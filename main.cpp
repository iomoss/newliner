#include <iostream>

int main()
{
    int n;
    while(std::cin >> std::hex >> n)
    {
        std::cout << std::uppercase << std::hex << n << std::endl;
    }
}
